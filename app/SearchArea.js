import React from 'react';

const SearchArea = (props) => {
    return (
        <section className="searchbox-wrap">
            <form action="" onSubmit={props.handleSubmit}>
                <div className="input-field">
                    <input placeholder="Search for a movie..." type="text" className="searchbox" onChange={props.handleChange} />
                </div>
            </form>
        </section>

    )
}

export default SearchArea;