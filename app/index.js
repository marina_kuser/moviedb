import React, { Component } from 'react';
import ReactDOM from 'react-dom';
require('./index.css');
import SearchArea from './SearchArea'
import MovieList from './MovieList'
import Pagination from './Pagination'
import MovieInfo from './MovieInfo'

class App extends Component {
    constructor() {
        super()
        this.state = {
            movies: [],
            searchTerm: '',
            totalResults: 0,
            currentPage: 1,
            currentMovie: null
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();

        fetch(`https://api.themoviedb.org/3/search/movie?api_key=f3e61228debbc60262a78bf5f2bf66d9&query=${this.state.searchTerm}`)
            .then(data => data.json())
            .then(data => {
                console.log(data);
                this.setState({ movies: [...data.results], totalResults: data.total_results })
            })
    }

    handleChange = (e) => {
        this.setState({ searchTerm: e.target.value })
    }

    nextPage = (pageNumber) => {
        fetch(`https://api.themoviedb.org/3/search/movie?api_key=f3e61228debbc60262a78bf5f2bf66d9&query=${this.state.searchTerm}&page=${pageNumber}`)
            .then(data => data.json())
            .then(data => {
                console.log(data);
                this.setState({ movies: [...data.results], currentPage: pageNumber })
            })
    }

    viewMovieInfo = (id) => {
        const filteredMovie = this.state.movies.filter(movie => movie.id == id)

        const newCurrentMovie = filteredMovie.length > 0 ? filteredMovie[0] : null

        this.setState({ currentMovie: newCurrentMovie })
    }

    closeMovieInfo = () => {
        this.setState({ currentMovie: null })
    }

    render() {
        const numberPages = Math.floor(this.state.totalResults / 20);

        return (
            <div>
                <div className="header">
                    <h1>Movie DB</h1>
                </div>
                {this.state.currentMovie == null ? <div><div className="searchInput"><SearchArea handleSubmit={this.handleSubmit} handleChange={this.handleChange} /></div><MovieList viewMovieInfo={this.viewMovieInfo} movies={this.state.movies} /></div> : <MovieInfo currentMovie={this.state.currentMovie} closeMovieInfo={this.closeMovieInfo} />}

                {this.state.totalResults > 20 && this.state.currentMovie == null ? <Pagination pages={numberPages} nextPage={this.nextPage} currentPage={this.state.currentPage} /> : ''}
            </div >
        );
    }
}
export default App;

ReactDOM.render(<App />, document.getElementById('app'))