import React from 'react'

const Movie = (props) => {
    return (
        <div className="col s12 m6 l3">
            <div className="card" onClick={() => props.viewMovieInfo(props.movieId)}>
                <div className="card-image waves-effect waves-block waves-light">
                    {
                        props.image == null ? <img src={`https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTb481OSt5pyxs0sDiBLtbJSYBqQDFijRlhf74kfk4Wtz5Qo5-z`} alt="card image" style={{ width: "100%", height: 360 }} /> : <img src={`http://image.tmdb.org/t/p/w185${props.image}`} alt="card image" style={{ width: "100%", height: 360 }} />
                    }
                </div>
                <div className="card-content">
                    <p style={{ fontWeight: "bold", fontSize: 14 }}>{props.titleMovie.length > 21 ? props.titleMovie.substring(0, 18) + '...' : props.titleMovie}</p>
                    <p class="blue-text" style={{ fontSize: 14 }}>{props.releaseDate == "" ? <p>No date</p> : props.releaseDate.substring(0, 4)}</p>
                </div>
            </div>
        </div>
    )
}

export default Movie
