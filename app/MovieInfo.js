import React from 'react';

const MovieInfo = (props) => {
    return (
        <div className="container grey-text text-lighten-5">
            <div className="row" onClick={props.closeMovieInfo} style={{ cursor: "pointer", paddingTop: 50 }}>
                <i className="fa fa-angle-left"></i>
                <span style={{ marginLeft: 10 }}>Go back</span>
            </div>
            <div className="row">
                <div className="col s12 m4">
                    {props.currentMovie.poster_path == null ? <img src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTb481OSt5pyxs0sDiBLtbJSYBqQDFijRlhf74kfk4Wtz5Qo5-z"} alt="card image" style={{ width: "100%", height: 360 }} />
                        : <img src={`http://image.tmdb.org/t/p/w185${props.currentMovie.poster_path}`} alt="card image" style={{ width: "100%", height: 360 }} />
                    }
                </div>
                <div className="col s12 m8">
                    <div className="info-container">
                        <p><span style={{ fontWeight: "bold", fontSize: 35 }}>{props.currentMovie.title}</span></p>
                        <p><span style={{ fontWeight: "bold", fontSize: 20 }}>Release Date:</span>{props.currentMovie.release_date.substring(5).split("-").concat(props.currentMovie.release_date.substring(0, 4)).join("/")}</p>
                        <p><span style={{ fontWeight: "bold", fontSize: 20 }}>Rating:</span>{props.currentMovie.vote_average}</p>
                        <p><span style={{ fontWeight: "bold", fontSize: 20 }}>Popularity:</span>{props.currentMovie.popularity}</p>
                        <p><span style={{ fontWeight: "bold", fontSize: 20 }}>Language:</span><span style={{ textTransform: "uppercase" }}>{props.currentMovie.original_language}</span></p>
                        <p><span style={{ fontWeight: "bold", fontSize: 20 }}>Overview:</span>{props.currentMovie.overview}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MovieInfo;


